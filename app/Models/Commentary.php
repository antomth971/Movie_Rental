<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commentary extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    // protected $with = ["films","user"];

    public $timestamps=false;

    public function films()
    {
        return $this->belongsTo(films::class);
    }
    public function user(){
        return $this->hasMany(user::class);
    }
}
