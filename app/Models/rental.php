<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    use HasFactory;

    protected $guarded = ["id", "rent_date", "return_date", "id_film", "id_user"];

    // protected $with = ["films", "user"];

    public $timestamps=false;

    public function films()
    {
        return $this->hasMany(films::class);
    }
    public function user()
    {
        return $this->belongsTo(films::class);
    }
}
