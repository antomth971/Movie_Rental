<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    //protected $with = ["actors", "rental", "director", "category"];

    public $timestamps=false;

    public function rental()
    {
        return $this->belongsToMany(Rental::class);
    }
    public function actors()
    {
        return $this->belongsToMany(Actor::class,'actor_film')->using(ActorFilm::class);
    }
    public function director()
    {
        return $this->belongsTo(director::class);
    }
    public function category()
    {
        return $this->belongsTo(category::class);
    }


    // $table->integer("available");
    // $table->integer("legal_age");
    // $table->foreignId("id_actor")->constrained()->cascadeOnDelete();
    // $table->foreignId("id_director")->constrained()->cascadeOnDelete();
    // $table->foreignId("id_category")->constrained()->cascadeOnDelete();

}
