<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    use HasFactory;
    protected $guarded = ["id", "name", "biography", "reward", "date_birth", "date_death"];

    //  protected $with = ["films"];

    public $timestamps=false;

    public function films()
    {
        return $this->belongsToMany(Film::class,'actor_film')->using(ActorFilm::class);
    }
}
