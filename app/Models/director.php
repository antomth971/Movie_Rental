<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    // protected $with = ["films"];

    public $timestamps=false;

    public function films()
    {
        return $this->hasMany(films::class);
    }
}
