<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\ActorFilm;
use App\Models\Category;
use App\Models\Film;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user=Auth::user();
        // return view('home');
        if (($user)!=null) {
        if ($user->role_id==1) {
           $vue='client.landingpage';
        } elseif ($user->role_id==2) {
            $vue='admin.landingpage';
        }
        elseif ($user->role_id==3) {
            $vue='sup_admin.landingpage';
    }}
        else {
            $vue='index';
        }
        return view($vue, ["film" => Film::with(["actors", "rental", "director", "category"])->get(),"user"=>User::all(),"categ"=>Category::all(),"actors"=>Actor::all(),"assosActor"=>ActorFilm::all()]);

    }
}
