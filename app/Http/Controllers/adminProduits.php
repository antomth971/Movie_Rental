<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Director;
use App\Models\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminProduits extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view("admin.landingpage", ["film" => Film::with(["actors", "rental", "director", "category"])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.admin_create",["categ" => Category::all(),"director" => Director::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'price' => 'required',
            'description'  => 'required',
            'year' => 'required',
            'img' => 'required',
            'available' => 'required | min:0|max:1',
            'legal_age' => 'required',
            'time' => 'required',
            'director_id' => 'required',
            'category_id' => 'required',
        ]);

        $nomImage = $request->file("img")->getClientOriginalName();

        $attributes["img"]=$request->file("img")->storeAs("img",$nomImage,"public");


        Film::create($attributes);

        return redirect('admins')->with('message', 'The film ' . $request->input('name') . ' have been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function show($film)
    {
        $lefilm = DB::table('directors')->select($columns=["directors.name as nameDir","films.name as name","description",'img',"price","year","available","legal_age","time","films.id as id","directors.id as idDir"])->join("films","films.director_id","=","directors.id")->where('films.id', '=', $film)->get();
        $actors = DB::table("actors")->select($columns=["name","id"])->join("actor_film","actor_film.actor_id","=","actors.id")->where("film_id","=",$film)->get();
        $lesCommentaires = DB::table("users")->select("*")->join("commentaries","commentaries.films_id","=","users.id")->where('films_id', '=', $film)->get();
        $countCommentaire=count($lesCommentaires);
        if ($countCommentaire==0) {
           $lesCommentaires=["name"=>"non","comment"=>"No comment for this film yet"];
        }
        return view("admin.admin_details", ["films" => $lefilm,"commentaires"=>$lesCommentaires,"actors"=>$actors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit($film)
    {
        $produit = Film::find($film);
        $lacateg = Film::where('id', '=', $film)->get("category_id");
        foreach ($lacateg as $key) {
            $lacateg2 = Category::where('id', '=', $key->category_id)->get();
        }
        $ledirector = Film::where('id', '=', $film)->get("director_id");
        foreach ($ledirector as $key) {
            $ledirector2 = Director::where('id', '=', $key->director_id)->get();
        }
        return view('admin.admin_update',['produit'=> $produit,"categ" => Category::all(),"director" => Director::all(),"lacateg2"=>$lacateg2,"ledirector2"=>$ledirector2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$film)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'price' => 'required',
            'description'  => 'required',
            'year' => 'required',
            'img' => 'required',
            'available' => 'required | min:0|max:1',
            'legal_age' => 'required',
            'time' => 'required',
            'director_id' => 'required',
            'category_id' => 'required',
        ]);

        if(isset($attributes["img"])){
            $nomImage = $request->file("img")->getClientOriginalName();

            $attributes["img"]=$request->file("img")->storeAs("img",$nomImage,"public");
        }

        $lefilm=Film::find($film);
        $lefilm->update($attributes);
        return redirect('admins')->with('message', 'The film ' . $request->input('name') . ' have been modified');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy( $film)
    {
        $lefilm = Film::find($film);

       $lefilm->delete();

       return redirect('/admins')->with('message','The film have been deleted');
    }
}
