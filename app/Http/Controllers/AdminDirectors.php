<?php

namespace App\Http\Controllers;

use App\Models\Director;
use Illuminate\Http\Request;

class AdminDirectors extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.director.index_director", ["directors" => Director::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.director.create_director");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'biography' => 'required',
            'reward' => 'required',
        ]);
        Director::create($attributes);

        return redirect('director')->with('message', "The director " . $request->input('name') . " have been added");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $directors = Director::find($id);
        return view("admin.director.details_director",["directors" => $directors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $director = Director::find($id);
        return view("admin.director.update_director",["director"=>$director]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'biography' => 'required',
            'reward'  => 'required',
        ]);
        $director=Director::find($id);
        $director->update($attributes);
        return redirect('director')->with('message', "The director " . $id . " have been modified");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $director = Director::find($id);

        $director->delete();

        return redirect('director')->with('message','The user '.$director->name.' have been deleted');
    }
}
