<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\ActorFilm;
use App\Models\Category;
use App\Models\Commentary;
use App\Models\Film;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function index(){
        return view('client.landingpage',["film" => Film::with(["actors", "rental", "director", "category"])->get(),"user"=>User::all(),"categ"=>Category::all(),"actors"=>Actor::all(),"assosActor"=>ActorFilm::all()]);
    }
    public function show($film){
        $lefilm = DB::table('directors')->select($columns=["directors.name as nameDir","films.name as name","description",'img',"price","year","available","legal_age","time","films.id as id","directors.id as idDir"])->join("films","films.director_id","=","directors.id")->where('films.id', '=', $film)->get();
        $actors = DB::table("actors")->select($columns=["name","id"])->join("actor_film","actor_film.actor_id","=","actors.id")->where("film_id","=",$film)->get();
        $lesCommentaires = DB::table("users")->select("*")->join("commentaries","commentaries.user_id","=","users.id")->where('films_id', '=', $film)->get();
        $countCommentaire=count($lesCommentaires);
        if ($countCommentaire==0) {
           $lesCommentaires=["name"=>"non","comment"=>"No comment for this film yet"];
        }
        return view("client.client_details", ["films" => $lefilm,"commentaires"=>$lesCommentaires,"actors"=>$actors]);
    }
    public function addComment($id){
        $name_film = DB::table('films')->select('name')->where('id',"=", $id)->get();
        return view('client.addComment',['id'=>$id,'name_film'=>$name_film]);
    }
    public function storeComment(Request $request){

        $attributes=$request->validate([
            'commentary' => 'required',
            'note'  => 'required',
            'films_id' => 'required',
            'user_id' => 'required',
        ]);

        $lefilm = DB::table('directors')->select($columns=["directors.name as nameDir","films.name as name","description",'img',"price","year","available","legal_age","time","films.id as id","directors.id as idDir"])->join("films","films.director_id","=","directors.id")->where('films.id', '=', intval($attributes['user_id']))->get();
        $actors = DB::table("actors")->select($columns=["name","id"])->join("actor_film","actor_film.actor_id","=","actors.id")->where("film_id","=",intval($attributes['user_id']))->get();
        $lesCommentaires = DB::table("users")->select("*")->join("commentaries","commentaries.films_id","=","users.id")->where('films_id', '=', intval($attributes['user_id']))->get();
        Commentary::create($attributes);

        return redirect()->route('test',intval($attributes['user_id']));//'client/'.intval($attributes['user_id']), ["films" => $lefilm,"commentaires"=>$lesCommentaires,"actors"=>$actors]);//('client/'.intval($attributes['user_id'])->with('message', "Your comment have been added");
    }


}
