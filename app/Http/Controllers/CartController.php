<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\ActorFilm;
use App\Models\Category;
use App\Models\Film;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cart;
use Darryldecode\Cart\Facades\CartFacade;
use PDF;


class CartController extends Controller
{
    public function CartList()
    {
       $CartItems = \Cart::getContent();
       return view('Cart', ['CartItems' => $CartItems]);
    }
    public function addToCart(Request $request)
    {
        \Cart::add([
            'id' =>$request->id,
            'name' =>$request->name,
            'price' =>$request->price,
            'quantity' =>$request->quantity,

            'attributes' =>array(
                'image' =>$request->image,
            )
        ]);

        return view('client.landingpage',["film" => Film::with(["actors", "rental", "director", "category"])->get(),"user"=>User::all(),"categ"=>Category::all(),"actors"=>Actor::all(),"assosActor"=>ActorFilm::all()])->with("message",'The film '.$request->name.' has been added  !');
    }

    public function updateCart(Request $request)
    {
        \Cart::update(
            $request->id,
            [
                'quantity' => [
                    'relative' =>false,
                    'value' =>$request->quantity
                ],
            ]
        );

        session()->flash('success', 'The card is update !');
        return redirect()->route('cart.list');
    }
    public function removeCart(Request $request)
    {
        \Cart::remove($request->id);
        session()->flash('success', 'The film has been removed !');
        return redirect()->route('cart.list');
    }

    public function clearAllCart()
    {
        \Cart::clear();

        session()->flash('success', 'The cart is clean !');
        return redirect()->route('cart.list');
    }



}
