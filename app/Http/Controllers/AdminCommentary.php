<?php

namespace App\Http\Controllers;

use App\Models\Commentary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminCommentary extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment=Commentary::find($id);
        $user=DB::table("commentaries")->select($columns=["users.id","roles.role"])->join("users","users.id","=","commentaries.user_id")->join("roles","roles.id","=","users.role_id")->where("commentaries.id","=",$id)->get();
        return view("admin.comment_update",["comment"=>$comment,"user"=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes=$request->validate([
            'commentary'  => 'required',
            'note' => 'required',
            'films_id'  => 'required',
            'user_id' => 'required',
        ]);
        $comment=Commentary::find($id);
        $comment->update($attributes);

        return redirect('admins')->with('message', 'The commentary N°'.$id.' (to the film n°'.$request->input('films_id').') have been modified');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Commentary::find($id);
        $comment->delete();

        return redirect('/admins')->with('message','The commentary N°'.$id.' (to the film n°'.$comment->films_id.') have been modified');
    }
}
