<?php

namespace App\Http\Controllers;

use App\Models\Rental;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cart;
use Darryldecode\Cart\Facades\CartFacade;
use DateInterval;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class RentalController extends Controller
{
    public function finalisationPanier(){
        $CardItems = \Cart::getContent()->toArray();
        view()->share('CardItems',$CardItems);
        $date = date("Y-m-d");
        $date = strtotime($date);
        $date = strtotime("+7 day", $date);
        DB::insert('insert into rentals (rent_date,return_date,id_client ) values (?,?,?)', [date("Y-m-d"),date("Y-m-d", $date),Auth::user()->id]);
        $idCommande = DB::select('SELECT id FROM rentals ORDER BY id DESC limit 1');

        foreach ($idCommande as $key => $value) {
            $idDeCommande = $value->id;
        }
        foreach ($CardItems as $key) {
        DB::insert('insert into film_rental (rental_id,film_id,quantity) values (?,?,?)', [$idDeCommande,$key['id'], $key['quantity']]);

        }
        \Cart::clear();

        session()->flash('success', 'Your order has been sent !');
        return redirect()->route('cart.list');

    }
    public function CreerPdf($id){
        $CardItems = DB::table('film_rental')->select($columns=["films.name","films.price","quantity"])->join("films","films.id","=","film_rental.film_id")->where("film_rental.rental_id","=",$id)->get()->toArray();
        view()->share('CardItems',$CardItems);
        $totalPrice=DB::table('film_rental')->select($columns=["price"])->join("films","id","=","film_rental.film_id")->where("rental_id","=",$id)->get();
        $tabPrice=[];
        foreach ($totalPrice as $key => $value) {
            array_push($tabPrice,$value->price);
        }
        $prix=array_sum($tabPrice);
        view()->share('prixtotal',$prix);
        $leId=Rental::find($id);
        $duClient=$leId->id_client;
        $idClient=User::find($duClient);
        view()->share("idClient",$idClient);
        $pdf = PDF::loadView('pdf');
        return $pdf->download('pdf_file.pdf');
    }
    public function indexFacture(){
        $idClient=Auth::user()->id;
        $commandes = Rental::where("id_client","=",$idClient)->orderBy("id","ASC")->get();
        return view('client.commandes',["commandes"=>$commandes]);
    }
    public function indexFactureAdmin(){
        $commandes = DB::table('rentals')->select($columns=["*","rentals.id as idRental"])->join("users","users.id","=","rentals.id_client")->orderBy("rentals.id","ASC")->get();
        foreach ($commandes as $key => $value) {
        }
        return view('admin.adminCommands',["commandes"=>$commandes]);
    }
    public function detailCommande($id){
        $donnee=DB::table('film_rental')->select("*")->join("films","id","=","film_rental.film_id")->where("rental_id","=",$id)->get();
        $totalPrice=DB::table('film_rental')->select($columns=["price"])->join("films","id","=","film_rental.film_id")->where("rental_id","=",$id)->get();
        $tabPrice=[];
        foreach ($totalPrice as $key => $value) {
            array_push($tabPrice,$value->price);
        }
        $prix=array_sum($tabPrice);
        $idClient=Auth::user()->id;
        return view("client.commandesDetail",["donnee"=>$donnee,"client"=>$idClient,"idCommande"=>$id,"prix"=>$prix]);
    }
    public function adminCommandDetail($id){
        $donnee=DB::table('film_rental')->select("*")->join("films","id","=","film_rental.film_id")->where("rental_id","=",$id)->get();
        $totalPrice=DB::table('film_rental')->select($columns=["price"])->join("films","id","=","film_rental.film_id")->where("rental_id","=",$id)->get();
        $tabPrice=[];
        foreach ($totalPrice as $key => $value) {
            array_push($tabPrice,$value->price);
        }
        $prix=array_sum($tabPrice);

        return view("admin.adminCommandDetail",["donnee"=>$donnee,"idCommande"=>$id,"prix"=>$prix]);
    }
}


//DB::table("rentals")->select($columns=["*"])->join("film_rental","film_rental.rental_id","=","rentals.id")->where("rentals.id_client","=",$idClient)->groupBy('rentals.id')->get();