<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class supAdmin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("sup_admin.landingpage", ["user" => user::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::get();
        return view("sup_admin.supAdmin_create",["roles" => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'surname' => 'required',
            'email'  => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        User::create($attributes);

        return redirect('supAdmin')->with('message', "The user " . $request->input('name') . " have been added");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $lefilm = User::where("id","=",$user)->get();
        return view("sup_admin.supAdmin_details", ["user" => $lefilm]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $user = User::find($user);
        $roles=Role::get();

        return view("sup_admin.supAdmin_update",["roles" => $roles,"user"=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$user)
    {
        $attributes=$request->validate([
            'name'  => 'required',
            'surname' => 'required',
            'email'  => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        $theUser=User::find($user);
        $theUser->update($attributes);
        return redirect('supAdmin')->with('message', "The user n°" . $user . "have been modified");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $lefilm = User::find($user);

        $lefilm->delete();

        return redirect('/supAdmin')->with('message','The user have been deleted');
    }
}
