<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\ActorFilm;
use App\Models\Category;
use App\Models\Commentary;
use App\Models\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Index extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("index", ["film" => Film::with(["actors", "rental", "director", "category"])->get(),"categ"=>Category::all(),"actors"=>Actor::all(),"assosActor"=>ActorFilm::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function show($film)
    {
        $lefilm = DB::table('directors')->select($columns=["directors.name as nameDir","films.name as name","description",'img',"price","year","available","legal_age","time","films.id as id","directors.id as idDir"])->join("films","films.director_id","=","directors.id")->where('films.id', '=', $film)->get();
        $actors = DB::table("actors")->select($columns=["name","id"])->join("actor_film","actor_film.actor_id","=","actors.id")->where("film_id","=",$film)->get();
        $lesCommentaires = DB::table("users")->select("*")->join("commentaries","commentaries.user_id","=","users.id")->where('films_id', '=', $film)->get();
        $countCommentaire=count($lesCommentaires);
        if ($countCommentaire==0) {
           $lesCommentaires=["name"=>"non","comment"=>"No comment for this film yet"];
        }
        return view("index_detail", ["films" => $lefilm,"commentaires"=>$lesCommentaires,"actors"=>$actors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit(Film $film)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Film $film)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        //
    }
}
