@extends('layouts.app')

@section('content')
<div class="row">
<h1 class="h1-admins">Your commands</h1>
</div>

<div class="container">
    <div class="col-6 bootsrapIndex form-group">
        <input type="text" class="form-control" placeholder="Search your command by id" name="search" id="search" onkeyup="commandSearch(this)">
        </div>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th>N°</th>
        <th>Rental date</th>
        <th>Return date</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
@foreach ($commandes as $laCommande)
<tr class="lesNoms" id="{{$laCommande->id}}" UserSearch="{{$laCommande->id}}">
    <td>{{$laCommande->id}}</td>
    <td>{{$laCommande->rent_date}}</td>
    <td>{{$laCommande->return_date}}</td>
    <td><a class="btn btn-primary" href="{{'/detail/commande/'.$laCommande->id}}">Detail</a></td>
  </tr>
@endforeach
    </tbody>
  </table>
</div>

@endsection