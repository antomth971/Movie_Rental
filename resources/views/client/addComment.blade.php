@extends('layouts.app')
@section('content')

<h1 class="h1-admins text-secondary">Added a new comment for the movie {{$name_film[0]->name}}</h1>
<div class="container">
<form action="/client/addComment/store" method="post">
@method('post')
@csrf
<div class="form-group mb-5">
<label for="note">Choose a note</label>
<select name="note" id="note" class="form-control selectpicker">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
</select>
</div>

<div class="form-group mb-5">
<label for="commentary">Write your comment</label>
<textarea name="commentary" id="commentary" placeholder="Great film ^^" class="form-control" cols="30" rows="10"></textarea>
</div>
<div class="form-group pb-3">
<input type="submit" value="Store your comment" class="form-control btn btn-success">
</div>
<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
<input type="hidden" name="films_id" value="{{$id}}">
</form>
</div>
@endsection