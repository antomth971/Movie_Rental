@extends('layouts.app')

@section('content')
<div class="container-fluid connexion_user"><p>Hello {{Auth::user()->name}} !!</p></div>
<div class="h1_div">
    <h1 class="text-center" style="font-size:60px">B-Movie</h1>
<h2 class="text-center mb-5" style="font-size: 25px">Hundreds of movies in high resolution</h2>
</div>
@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
@if ($message = Session::get('success'))
<div class='alert alert-success'role='alert'>
  <p>{{$message}}</p>
</div>
@endif
<div class="container">
    <div class="row mb-4">
<div class="col-3 bootsrapIndex">
    <select id="categoryIndex" class="selectpicker" data-live-search='true' onchange="categoryIndex(this)">
        <option value selected disabled>Choose a category</option>
        @foreach ($categ as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="col-3 bootsrapIndex">
    <select id="actorIndex" class="selectpicker" data-live-search='true' onchange="actorIndex(this)">
        <option value selected disabled>Choose a Actor</option>
        @foreach ($actors as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="col-6 bootsrapIndex form-group">
    <input type="text" class="form-control" placeholder="Search your film" name="search" id="search" onkeyup="filmSearch(this)">
</div>
    </div>
    <div class="row">
@foreach ($film as $item)
{{-- @foreach ($assosActor as $items)
    @if ($items->film_id==$item->id)
    <input type="hidden" class="test" value="{{$items->actor_id}}">
    @endif

    @endforeach --}}
<div id="{{$item->id}}" class="col categCard laCard mb-5" dataNameSearch="{{$item->name}}"  dataActor="" dataCateg="{{$item->category_id}}">
<div class="card" style="width: 18rem;height:30rem;">
    <img class="card-img-top img_index" src="images/{{$item->img}}.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title text-center">{{$item->name}}</h5>
      <p class="card-text card-text-index">{{$item->description}}</p>
      <div class="row">
          <div class="col-6"><a href="{{ URL::to('client/' . $item->id) }}" class="btn btn-primary">Detail</a></div>
            <div class="col-6 ">
                <form action="{{ route('cart.ajout') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <input type="hidden" value="{{$item->id}}" name="id">
                    <input type="hidden" value="{{$item->name}}" name="name">
                    <input type="hidden" value="{{$item->price}}" name="price">
                    <input type="hidden" value="{{$item->img}}" name="image">
                    <input type="hidden" value="1" name="quantity">
                    <button class="btn text-align" style="background-color: #bdc3c7;color:white">Add to cart</button>
                </form>

      </div>
      </div>

    </div>
  </div>
</div>
@endforeach
</div>
<h3 > </h3>
</div>




@endsection