@extends('layouts.app')

@section('content')
<div class="h1_div">
    <h1 class="text-center" style="font-size:60px">B-Movie</h1>
<h2 class="text-center mb-5" style="font-size: 25px">Hundreds of movies in high resolution</h2>
</div>
<div class="container">
    <div class="row mb-4">
<div class="col-3 bootsrapIndex">
    <select id="categoryIndex" class="selectpicker" data-live-search='true' onchange="categoryIndex(this)">
        <option value selected disabled>Choose a category</option>
        @foreach ($categ as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="col-3 bootsrapIndex">
    <select id="actorIndex" class="selectpicker" data-live-search='true' onchange="actorIndex(this)">
        <option value selected disabled>Choose a Actor</option>
        @foreach ($actors as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="col-6 bootsrapIndex form-group">
<input type="text" class="form-control" placeholder="Search your film" name="search" id="search" onkeyup="filmSearch(this)">
</div>
    </div>
    <div class="row">
@foreach ($film as $item)
<div id="{{$item->id}}" class="col categCard laCard mb-5" dataNameSearch="{{$item->name}}" dataCateg="{{$item->category_id}}">
<div class="card" style="width: 18rem; height:30rem">
    <img class="card-img-top img_index" src="images/{{$item->img}}.jpg" alt="Image film n° {{$item->id}}">
    <div class="card-body">
      <h5 class="card-title text-center">{{$item->name}}</h5>
      <p class="card-text card-text-index">{{$item->description}}</p>
      <p class="card-text"><b> Category : </b>{{$item->category->name}}</p>
      <a href="{{ URL::to('films/' . $item->id) }}" class="btn btn-primary">Detail</a>
    </div>
  </div>
</div>
@endforeach
</div>
</div>




@endsection