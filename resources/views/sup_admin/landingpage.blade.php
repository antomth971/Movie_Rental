@extends("layouts.app")
@section('content')
<div class="container">
    <h1> Hello super administrateur {{Auth::user()->name}}</h1>
</div>

<div class="container">
    @if (Session::has('message'))
    <div class="alert alert-succes">
        {{ Session::get('message') }}
    </div>
    @endif

<div class="row">
    <div class="col-6 bootsrapIndex form-group">
    <input type="text" class="form-control" placeholder="Search your user by id or by name" name="search" id="search" onkeyup="userSearch(this)">
    </div>
</div>
    <table class="table borderWhite mt-3">
        <thead>
            <tr>

                <th>Users</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>

        @foreach ($user as $item)
                <tr class="lesNoms" id="{{$item->name." ".$item->id}}" UserSearch="{{$item->id}}">
                    <td>{{ $item->name }} (Id n°{{$item->id}})</td>
                    <td><a href="{{ URL::to('supAdmin/' . $item->id) }}" class="btn btn-primary">Detail</a></td>
                </tr>
        @endforeach
        </tbody>
    </table>









</div>



@endsection
