@extends('layouts.app')



@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li> {{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<center>
<h1 class="h1-admins">Update user N°{{$user->id}}</h1>
</center>

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<div class="container">
<form action="/supAdmin/{{$user->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method("put")
<div class="form-group">
<label for="name">Name</label>
<input type="text" class="form-control" name="name" id="name"  value="{{$user->name}}" placeholder="Enter a name">
</div>

<div class="form-group">
    <label for="surname">Surname</label>
    <input type="text" class="form-control" name="surname" id="surname"  value="{{$user->surname}}" placeholder="Enter a surname">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" id="email"  value="{{$user->email}}" placeholder="Enter a email">
        </div>


        <div class="form-group mt-4">
            <label for="role_id">Role</label>
                <select name="role_id" class="selectpicker" data-live-search="true" id="role_id">
                    @foreach ($roles as $item)
                    @if ($item->id == $user->role_id)
                    <option value="{{$item->id}}" disabled selected>{{$item->role}}</option>
                @endif
                    <option value="{{$item->id}}">{{$item->role}}</option>
                    @endforeach
                </select>
        </div>

<input type="hidden" name="password" value="{{$user->password}}">
        <div class="row mt-3">
<div class="form-group">
<input type="submit" value="Update" class="btn btn-secondary form-control">
</div>
</div>
</form>
</div>
@endsection
