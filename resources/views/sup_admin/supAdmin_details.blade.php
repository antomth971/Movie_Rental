@extends("layouts.app")
@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<div class="container">
<div class="row">
@foreach ($user as $item)
<h1 class="text-center">Information of Personnel N°{{$item->id}}</h1>

<table class="table borderWhite mt-3 ">

    <thead>
        <tr>

            <th>List</th>
            <th></th>

        </tr>
    </thead>
        <tbody>
            <tr>
                <td>Name</td>
                <td class="text-center">{{ $item->name }}</td>
    </tr>
    <tr>
        <td>Surname</td>
        <td class="text-center">{{ $item->surname}}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td class="text-center">{{ $item->email}}</td>
    </tr>
    <tr>
        <td>Role</td>
        @if ($item->role_id==1)
        <td class="text-center">Client</td>
        @elseif ($item->role_id==2)
        <td class="text-center">Administrateur</td>
        @else
        <td class="text-center">Super Administrateur</td>
        @endif
    </tr>
    </tbody>
</table>


@endforeach
</div>

<div class="row">
    <div class="row">
        <div class="col-6 pb-3">
    <a href="{{ $item->id }}/edit" class="btn btn-warning d-flex justify-content-center">Update</a>
</div>
    <div class="col-6 pb-3">
        <form action="/supAdmin/{{$item->id}}" method="post">
            @csrf
            @method("delete")
            <input type="submit" class="btn btn-danger form-control" value="Delete" onClick='return confirm("Are you sure to delete {{$item->name}}?")'>
            </form>
</div>
</div>


</div>
@endsection
