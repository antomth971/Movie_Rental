@extends('layouts.app')



@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li> {{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h1 class="h1-admins">Create an user</h1>

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<div class="container">
<form action="/supAdmin" method="post" enctype="multipart/form-data">
    @csrf
    @method("post")
<div class="form-group">
<label for="name">Name</label>
<input type="text" class="form-control" name="name" id="name" placeholder="Enter a name">
</div>

<div class="form-group">
    <label for="surname">Surname</label>
    <input type="text" class="form-control" name="surname" id="surname" placeholder="Enter a surname">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" id="email" placeholder="Enter a email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Enter a password">
        </div>

        <div class="form-group mt-4">
            <label for="role_id">Role</label>
                <select name="role_id" class="selectpicker" data-live-search="true" id="role_id">
                    <option value disabled selected>Select a role</option>
                    @foreach ($roles as $item)
                    <option value="{{$item->id}}">{{$item->role}}</option>
                    @endforeach
                </select>
        </div>


        <div class="row mt-3">
<div class="form-group">
<input type="submit" value="Store" class="btn btn-secondary">
</div>
</div>
</form>
</div>
@endsection
