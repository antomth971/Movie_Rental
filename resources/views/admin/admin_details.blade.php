@extends("layouts.app")
@section('content')

<div class="container-fluid">
    <div class="row">
    <div class="d-flex justify-content-center mx-auto">


        @foreach ($films as $item)
            <div class="card" style="width: 45rem;height:30rem">
                <img class="card-img-top img_card_detail_admin" src="/images/{{ $item->img }}.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <p class="card-text">{{ $item->description }}</p>
                    <p class="card-text"><b>{{ $item->price }}€</b></p>
                    <div class="row">
                        <div class="col-6">
                    <a href="{{ $item->id }}/edit" class="btn btn-warning d-flex justify-content-center">Update</a>
                </div>
                    <div class="col-6">
                        <form action="/admins/{{$item->id}}" method="post">
                            @csrf
                            @method("delete")
                            <input type="submit" class="btn btn-danger form-control" value="Delete" onClick='return confirm("Are you sure to delete {{$item->name}}?")'>
                            </form>
                </div>
                </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
    <div class="row">
        <h2 class="ml-0">Commentaries</h2>
                @if (!isset($commentaires["name"]))
                    @foreach ($commentaires as $leCommentaire)
                    <div class="col-6">

                <div>
                    <div class="card  mb-4">
                        <div class="card-body">
                            <h5 class="card-title"><b> User : </b> {{$leCommentaire->name}} <b>{{ $leCommentaire->note }}/5</b> </h5>
                            <p class="card-text"><b> Commentaire : </b>{{ $leCommentaire->commentary }}</p>
                            <div class="row">
                                <div class="col-6">
                            <a href="/comment/{{ $leCommentaire->id }}/edit" class="btn btn-warning d-flex justify-content-center">Update</a>
                        </div>
                            <div class="col-6">
                                <form action="/comment/{{$leCommentaire->id}}" method="post">
                                    @csrf
                                    @method("delete")
                                    <input type="submit" class="btn btn-danger form-control" value="Delete" onClick='return confirm("Are you sure to delete the commentaire n°{{$leCommentaire->id}}?")'>
                                    </form>
                        </div>
                        </div>
                        </div>
                    </div>


                </div>
            </div>
                @endforeach
                @else

                <h2 class="text-center text-danger p-2">{{$commentaires["comment"]}}</h2>
                @endif



    </div>
</div>



@endsection