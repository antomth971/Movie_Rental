@extends("layouts.app")
@section('content')
<div class="container">
<div class="row mb-5">
<h1 class="h1-admins mb-5">Update Commentary N°{{$comment->id}}</h1>
<h3 class="text-white">User : @foreach ($user as $item)
N°{{$item->id}} ({{$item->role}})
@endforeach</h3>
</div>
<form action="/comment/{{$comment->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method("put")


<div class="form-group mb-5">
    <label for="commentary">Commentary</label>
    <input type="text" class="form-control" name="commentary" id="commentary" placeholder="Modify the comment"cols="30" rows="10">{{$comment->commentary}}</textarea>
    </div>

    <div class="form-group mb-4">
        <label for="note">Note</label>
        <select name="note" id="note" class="selectpicker">
            <option value="{{$comment->note}}" selected disabled>{{$comment->note}}</option>
    @for ($i = 0; $i <= 5; $i++)
    <option value="{{$i}}">{{$i}}</option>
    @endfor
        </select>
    </div>
    <input type="hidden" name="films_id" value="{{$comment->films_id}}">
    <input type="hidden" name="user_id" value="{{$comment->user_id}}">


        <input type="submit" value="Update" class="btn btn-secondary form-control mb-3 mt-3">
    </div>
    </form>
</div>

@endsection