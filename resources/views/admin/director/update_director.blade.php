@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif

<h1 class="h1-admins">Update director N°{{$director->id}}</h1>

<div class="container mb-3">
    <form action="/director/{{$director->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method("put")
    <div class="form-group mb-3">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" id="name"  value="{{$director->name}}" placeholder="Enter a name">
    </div>

    <div class="form-group  mb-3">
        <label for="biography">Biography</label>
        <textarea type="text" class="form-control" name="biography" id="biography" placeholder="Enter a biography"cols="30" rows="10">{{$director->biography}}</textarea>
        </div>
        <div class="form-group  mb-3">
            <label for="reward">Reward</label>
            <input type="text" class="form-control" name="reward" id="reward"  value="{{$director->reward}}" placeholder="Enter a reward">
            </div>

    <div class="row mt-3  mb-3">
    <div class="form-group">
    <input type="submit" value="Update" class="btn btn-secondary form-control">
    </div>
    </div>
    </form>
    </div>



@endsection