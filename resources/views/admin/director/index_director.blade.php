@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<div class="container">
<h1 class="h1-admins">Update Director</h1>

<div class="row">
    <div class="col-6 bootsrapIndex form-group">
    <input type="text" class="form-control" placeholder="Search your director by id or by name" name="search" id="search" onkeyup="userSearch(this)">
    </div>
</div>


<table class="table borderWhite mt-3">
    <thead>
        <tr>

            <th>Directors</th>
            <th>Detail</th>
        </tr>
    </thead>
    @foreach ($directors as $item)
        <tbody>
            <tr class="lesNoms" id="{{$item->name." ".$item->id}}" UserSearch="{{$item->id}}">
                <td>{{ $item->name }} (Director n°{{$item->id}})</td>
                <td><a href="{{ URL::to('director/' . $item->id) }}" class="btn btn-primary">Detail</a></td>
    @endforeach
    </tr>
    </tbody>
</table>

</div>


@endsection