@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif

<h1 class="h1-admins">Create new director</h1>

<div class="container mt-5">
    <form action="/director" method="post" enctype="multipart/form-data">
        @csrf
        @method("post")
    <div class="form-group mb-3">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Enter a name">
    </div>

    <div class="form-group mb-3">
        <label for="biography">Biography</label>
        <textarea name="biography" class="form-control" id="biography" cols="30" rows="10" placeholder="Enter a biography"></textarea>
    </div>
    <div class="form-group mb-3">
        <label for="reward">Reward</label>
        <input type="text" class="form-control" name="reward" id="reward" placeholder="Enter reward ?">
    </div>

            <div class="row mt-3">
    <div class="form-group">
    <input type="submit" value="Store" class="btn btn-secondary">
    </div>
    </div>
    </form>
    </div>



@endsection