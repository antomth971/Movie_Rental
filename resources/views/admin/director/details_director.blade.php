@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<h2 class="h1-admins">Detail of director n°{{$directors->id}}</h2>
<div class="d-flex justify-content-center card_up">
<div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">{{$directors->name}}</h5>
        <h6>Biography</h6>
      <p class="card-text">{{$directors->biography}}</p>
      <h6>Reward</h6>
      <p class="card-text">{{$directors->reward}}</p>
      <div class="row">
        <div class="col-6">
    <a href="/director/{{ $directors->id }}/edit" class="btn btn-warning d-flex justify-content-center">Update</a>
</div>
    <div class="col-6">
        <form action="/director/{{$directors->id}}" method="post">
            @csrf
            @method("delete")
            <input type="submit" class="btn btn-danger form-control" value="Delete" onClick='return confirm("Are you sure to delete {{$directors->name}}?")'>
            </form>
</div>
</div>
    </div>
  </div>
</div>




@endsection