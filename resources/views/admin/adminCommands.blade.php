@extends('layouts.app')

@section('content')
<div class="row">
<h1 class="h1-admins">Commands of clients</h1>
</div>
<div class="container">
    <div class="col-6 bootsrapIndex form-group">
        <input type="text" class="form-control" placeholder="Search your command by id" name="search" id="search" onkeyup="commandSearch(this)">
        </div>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th>N°</th>
        <th>Curstomers</th>
        <th>Rental date</th>
        <th>Return date</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
@foreach ($commandes as $laCommande)
<tr class="lesNoms" id="{{$laCommande->id." ".$laCommande->name}}" UserSearch="{{$laCommande->id}}">
    <td>{{$laCommande->idRental}}</td>
    <td>{{$laCommande->name}} (N°{{$laCommande->id_client}})</td>
    <td>{{$laCommande->rent_date}}</td>
    <td>{{$laCommande->return_date}}</td>
    <td><a class="btn btn-primary" href="{{'/detail/commandAdmin/'.$laCommande->idRental}}">Detail</a></td>
  </tr>
@endforeach
    </tbody>
  </table>
</div>

@endsection