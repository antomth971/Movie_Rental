@extends('layouts.app')



@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li> {{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<center>
<h1 class="h1-admins">Update film : {{$produit->name}}</h1>
</center>

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
{{-- @dd() --}}
<div class="container">
<form action="/admins/{{$produit->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method("put")
<div class="form-group">
<label for="name">Name</label>
<input type="text" class="form-control" name="name" id="name" value="{{$produit->name}}" placeholder="Enter a name">
</div>

<div class="form-group">
    <label for="price">Price</label>
    <input type="text" class="form-control" name="price" id="price" value="{{$produit->price}}" placeholder="Enter a price">
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" name="description" id="description" value="{{$produit->description}}" placeholder="Enter a description">
        </div>

        <div class="form-group">
            <label for="year">Year</label>
            <input type="number" min="1900" name="year" class="form-control" value="{{$produit->year}}" id="year" max="2022" step="1" value="2022" placeholder="Select a Year">
        </div>
        <div class="form-group">
            <label for="img">Image</label>
            <input type="file" class="form-control taille" class="form-control" name="img" id="img" placeholder="Choose a img">
            </div>
            <div class="form-group">
                <label for="available">Available</label>
                <input type="number" min="0" name="available" class="form-control" id="available" value="{{$produit->available}}" max="1" step="1" placeholder="Select if it's available ">
            </div>
            <div class="form-group">
                <label for="legal_age">Legal_age</label>
                <input type="number" min="3" name="legal_age" class="form-control" id="legal_age" max="18" step="1" value="{{$produit->legal_age}}" placeholder="Select an legal age ">
            </div>
            <div class="form-group">
                <label for="time">Time</label>
                <input type="time" name="time" id="time" class="form-control" value="{{$produit->time}}" placeholder="Select the time of the film ">
            </div>
            <div class="row mt-3">
            <div class="form-group">
                <label for="director_id">Director</label>
                <select name="director_id" id="director_id" class="selectpicker" data-live-search="true">
                    @foreach ($ledirector2 as $item)
                    <option value="{{$item->id}}"  selected>{{$item->name}}</option>
                    @endforeach
                    @foreach ($director as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- @dd($lacateg2) --}}
        <div class="row mt-3">
            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" id="category_id" class="selectpicker" data-live-search="true">
                    @foreach ($lacateg2 as $item)
                    <option value="{{$item->id}}" selected >{{$item->name}}</option>
                    @endforeach
                    @foreach ($categ as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="row mt-3">
<div class="form-group">
    <input type="hidden" name="id" value="{{$produit->id}}">
<input type="submit" value="Update" class="btn btn-secondary form-control">
</div>
</div>
</form>





</div>
@endsection
