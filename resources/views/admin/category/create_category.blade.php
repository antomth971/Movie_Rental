@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif

    <h1 class="h1-admins">Create a new category</h1>

    @if (Session::has('message'))
    <div class="alert alert-succes">
        {{ Session::get('message') }}
    </div>
    @endif
    <div class="container">
    <form action="/category" method="post" enctype="multipart/form-data">
        @csrf
        @method("post")
    <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Enter a name for the now category">
    </div>

            <div class="row mt-3 pb-3">
    <div class="form-group">
    <input type="submit" value="Store" class="btn btn-secondary">
    </div>
    </div>
    </form>
    </div>




@endsection