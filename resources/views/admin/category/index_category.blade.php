@extends("layouts.app")

@section("content")

@if (Session::has('message'))
<div class="alert alert-succes">
    {{ Session::get('message') }}
</div>
@endif
<div class="container">
<h1 class="h1-admins">Update categories</h1>

<div class="row">
    <div class="col-6 bootsrapIndex form-group">
    <input type="text" class="form-control" placeholder="Search your category by id or by name" name="search" id="search" onkeyup="userSearch(this)">
    </div>
</div>


<table class="table borderWhite mt-3">
    <thead>
        <tr>

            <th>Categories</th>
            <th>Delete</th>
        </tr>
    </thead>
    @foreach ($categories as $item)
        <tbody>
            <tr class="lesNoms" id="{{$item->name." ".$item->id}}" UserSearch="{{$item->id}}">
                <td>{{ $item->name }} (Category n°{{$item->id}})</td>
                <td><form action="/category/{{$item->id}}" method="post">
                    @csrf
                    @method("delete")
                    <input type="submit" class="btn btn-danger " value="Delete" onClick='return confirm("Are you sure to delete the category{{$item->name}}?")'>
                    </form></td>
    @endforeach
    </tr>
    </tbody>
</table>

</div>

@endsection