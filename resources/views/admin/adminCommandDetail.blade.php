@extends('layouts.app')

@section('content')
<div class="row mb-4">
<h1 class="h1-admins">Command N°{{$idCommande}}</h1>
</div>

<div class="container mb-4">
    <div class="row">
    <div class="col-6 p-3">
        <a href="/commandsAdmin" class="btn btn-primary">Return to commands of customers</a>
    </div>
    <div class="col-6 p-3">
        <a class="btn btn-secondary" href="/pdfClient/{{$idCommande}}">Download invoice</a>
        </div>
    </div>
<table class="table borderWhite">
    <thead class="thead-dark">
      <tr>
        <th></th>
        <th>Name</th>
        <th> Quantity </th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
@foreach ($donnee as $laCommande)
<tr>
    <td><img src="/images/{{$laCommande->img}}.jpg" alt="img film" class="rounded w-25"></td>
    <td>{{$laCommande->name}}</td>
    <td>{{$laCommande->quantity}}</td>
    <td>{{$laCommande->price}} €</td>
  </tr>
@endforeach
    </tbody>
  </table>
</div>
<div class="container">
    <div style="float: right">
    <table class="borderWhite">
        <tr>
            <td class="">Price :</td>
            <td>{{$prix}} €</td>
        </tr>
    </table>
</div>
</div>
@endsection