@extends("layouts.app")
@section('content')
<div class="container">
    <h1>Hello Administrateur {{Auth::user()->name}}</h1>
    @if (Session::has('message'))
    <div class="alert alert-succes">
        {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-6 bootsrapIndex form-group">
        <input type="text" class="form-control" placeholder="Search your film by id or by name" name="search" id="search" onkeyup="userSearch(this)">
        </div>
    </div>


    <table class="table borderWhite mt-3">
        <thead>
            <tr>

                <th>Films</th>
                <th>Detail</th>
            </tr>
        </thead>
        @foreach ($film as $item)
            <tbody>
                <tr class="lesNoms" id="{{$item->name." ".$item->id}}" UserSearch="{{$item->id}}">
                    <td>{{ $item->name }} (Film n°{{$item->id}})</td>
                    <td><a href="{{ URL::to('admins/' . $item->id) }}" class="btn btn-primary">Detail</a></td>
        @endforeach
        </tr>
        </tbody>
    </table>









</div>

@endsection
