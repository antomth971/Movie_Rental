<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{date("d.m.y")}}</title>
</head>
<body>
<div class="container">
        <div class="">
            <h3>B_Movie</h3>
            <p>Name : Anto</p>
            <p>Adress : 123 street Lorem ipsum dolor sit amet consectetur.</p>
            <p>Siren : 123 456 789</p>
            <p>Phone :  0X XX XX XX XX</p>
        </div>
<br>

        <div class="divClient">
            <h3>Customer</h3>
            <p>Name : {{$idClient->name}}</p>
            <p>Email : {{$idClient->email}}</p>
        </div>
<br>
<div class="row">
    <h2>Invoice from {{$idClient->name}}</h2>
</div>
<br>
    <div class="tableauProduit">
        <table class="table borderWhite">
            <thead>
              <tr>
                <th class="borderWhite text-white">Products of the order</th>
                <th class="borderWhite text-white">Quantity</th>
                <th class="borderWhite text-white">Price</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($CardItems as $item)
                <tr>

                    <td>{{$item->name}}</td>
                    <td>{{$item->quantity}}</td>
                    <td>{{$item->price}}</td>
                </tr>

                @endforeach
            </tbody>
          </table>
    </div>
<br>
    <div class="row text-end">
        <h4> <span class="bg-dark text-white ">Total</span> {{$prixtotal }} €  </h4>
    </div>



</div>


<style>
    /* .divClient{
        float: right;
    } */

    .row{
        margin-top: 30px;
    }
    .bg-dark{
        background-color: grey;
    }
    .text-white{
        color: grey;
    }
    .text-end{
        display: flex;
        justify-content: end;
    }
    .divClient{
        margin-left: 70%;
    }
    .text-white{
        color: black;
    }
    .borderWhite{
    border: solid 3px;
    border-color: black;
}

</style>
</body>
</html>