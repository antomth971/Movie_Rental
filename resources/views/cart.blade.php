@extends("layouts.app")
@section('content')
  <div class="container">
    @if ($message = Session::get('success'))
      <div class='alert alert-success'role='alert'>
        <p>{{$message}}</p>
      </div>
    @endif
    <div class="container-fluid">
        <a href="{{ url('/home') }}" class="btn btn-primary mt-3">Continue your shopping</a>
    </div>
    <h1 class="text-center my-5 mt-5">Cart</h1>
 <table class="table table-warning">
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">Name</th>
          <th scope="col"> Quantity </th>
          <th scope="col">Price</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($CartItems as $item)

        <tr>
            <td>
              <img src="images/{{$item->attributes->image}}.jpg"class="img-thumbnail rounded w-50"alt="Thumbnail">
            </td>
            <td>
              {{$item->name}}
            </td>
            <td>
              <form action="{{route('cart.modif') }}"method="POST">
                  @csrf
                  <input type="hidden"name="id"value="{{$item->id}}">
                  <input type="number"name="quantity" class="quantity" value="{{$item->quantity}}"class="text-center">
            </td>

        </form>
            <td>
              {{$item->price * $item->quantity}} €
            </td>
            <td>
              <form action="{{route('cart.retirer') }}"method="POST">
                @csrf
                <input type="hidden"value="{{$item->id}}"name="id">
                <button class="btn btn-danger btn-sm">X</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-end fs-1">
      Total: {{Cart::getTotal() }} €
    </div>
    <div class="row mt-3 text-end">
        <div class="align-itemx">
        <div class="col">
      <form action="{{route('cart.vider') }}"method="POST">
          @csrf
          <button class="btn btn-lg btn-secondary">Clear all cart</button>
      </form>
    </div><div class="col p-2">
      <a href="{{URL::to('/valider')}}" class="btn btn-primary">Confirm your cart</a>
    </div>
    </div>
</div>
  </div>
@endsection
