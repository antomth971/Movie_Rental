@extends('layouts.app')
@section('content')
    <div class="container">

            @foreach ($films as $item)
<div class="row mt-4">
<div class="col-4">
    <img src="/images/{{$item->img}}.jpg" height="100%" width="100%" alt="img film"></div>
<div class="col-4">
    <div class="row text-center">
      <h1>  {{$item->name}}</h1>
    </div>
    <div class="row ml-2 mt-5">Year : {{$item->year}}</div>
    <div class="row mt-5"><h3>Synopsis</h3>{{$item->description}}</div>
    <div class="row mt-5"><div class="col-4"><strong>Time</strong></div><div class="col-8 text-center">{{$item->time}}</div></div>
    <div class="row mt-5"><div class="col-4"><strong>Legal age</strong></div><div class="col-8 text-center">{{$item->legal_age}}</div></div>

    <div class="row mt-5">
        <h6>if you wish to purchase, log in or create an account</h6>
        <div class="row">
            <div class="col-6"><a href="{{ url('/login') }}" class="btn btn-secondary d-flex justify-content-center">Login</a></div>
            <div class="col-6"><a href="{{ url('/register') }}" class="btn btn-secondary d-flex justify-content-center">Register</a></div>
    </div>
    </div>
</div>
</div>

            @endforeach
            <div class="row">
                <h2 class="ml-0">Commentaries</h2>
                <h3 class="text-center bg-light rounded">If you want to add a new comment, <a href="/client/addComment/{{$item->id}}">click here</a></h3>
            <div class="col-6">
                @foreach ($commentaires as $leCommentaire)
            <div >
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title"><b> User : </b> {{$leCommentaire->name}} <b>{{ $leCommentaire->note }}/5</b> </h5>
                        <p class="card-text"><b> Commentaire : </b>{{ $leCommentaire->commentary }}</p>
                    </div>
                </div>
            </div>
            @endforeach
            </div>
            </div>
    </div>
@endsection