<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ActorFilmFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'actor_id'=>$this->faker->numberBetween($min = 1, $max = 30),
            'film_id'=>$this->faker->numberBetween($min = 1, $max = 200),
        ];
    }
}
