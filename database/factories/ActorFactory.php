<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ActorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name(),
            "biography" => $this->faker->paragraph($nbSentences = 3,$variableNbSentences = true),
            "reward" => $this->faker->sentence($nbWords = 3, $variableNbWords = true),
            "date_birth" => $this->faker->date($format = 'Y-m-d', $max = '2000-01-01'),
            "date_death" => $this->faker->date($format = 'Y-m-d', $max = 'now'),
        ];
    }
}
