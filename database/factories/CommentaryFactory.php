<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentaryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "commentary" => $this->faker->paragraph($nbSentences = 2, $variableNbSentences = true),
            "note" => $this->faker->numberBetween($min = 1, $max = 5),
            "films_id"=>$this->faker->numberBetween($min = 1, $max = 200),
            "user_id"=>$this->faker->numberBetween($min = 1, $max = 60),
        ];
    }
}
