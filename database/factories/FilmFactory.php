<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class FilmFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=> $this->faker->word(),
            'description'=>$this->faker->paragraph($nbSentences = 4,$variableNbSentences = true),
            'price'=>$this->faker->numberBetween($min = 10, $max = 50),
            'year'=>$this->faker->year($max = 'now'),
            'available'=>$this->faker->numberBetween($min = 1, $max = 1),
            'legal_age'=>$this->faker->numberBetween($min = 8, $max = 21),
            'img'=> $this->faker->numberBetween($min = 1, $max = 23),
            'time'=> $this->faker->time($format = 'H:i:s'),
            'director_id'=>$this->faker->numberBetween($min = 1, $max = 20),
            'category_id'=>$this->faker->numberBetween($min = 1, $max = 10),
        ];
    }
}
