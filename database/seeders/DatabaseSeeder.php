<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Role;
use App\Models\User;

use DateTime;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Role::create(["role" => "client"]);
        Role::create(["role" => "admin"]);
        Role::create(["role" => "sup_admin"]);

        Category::create(["name" => "action"]);
        Category::create(["name" => "comedy"]);
        Category::create(["name" => "horror"]);
        Category::create(["name" => "sad"]);
        Category::create(["name" => "love"]);
        Category::create(["name" => "fantastic"]);
        Category::create(["name" => "plot"]);
        Category::create(["name" => "animation"]);
        Category::create(["name" => "paranormal"]);
        Category::create(["name" => "documentary"]);

        \App\Models\Actor::factory(30)->create();
        \App\Models\Director::factory(20)->create();
        \App\Models\Film::factory(200)->create();
        \App\Models\User::factory(60)->create();
        \App\Models\Commentary::factory(300)->create();




        User::create([
            "name"=>"Anthony",
            "surname"=>"Mathieu",
            "role_id"=>2,
            "email"=>"anthonymathieu@gmail.com",
            "password"=>bcrypt("12345678"),
        ]);
        User::create([
            "name"=>"anto",
            "surname"=>"math",
            "role_id"=>3,
            "email"=>"anthonymathieu@supadmin.com",
            "password"=>bcrypt("12345678"),
        ]);
        \App\Models\ActorFilm::factory(1000)->create();
        // \App\Models\User::factory(10)->create();
    }
}
