<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("description",$length=500);
            $table->float("price");
            $table->year("year");
            $table->integer("available");
            $table->integer("legal_age");
            $table->string('img');
            $table->time('time');
            $table->foreignId("director_id")->constrained("directors","id")->onDelete("cascade");
            $table->foreignId("category_id")->constrained("categories","id")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
