<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_rental', function (Blueprint $table) {
            $table->foreignId('rental_id')->constrained('rentals','id')->onDelete('cascade');
            $table->foreignId('film_id')->constrained('films','id')->onDelete('cascade');
            $table->integer("quantity");
            $table->primary(['rental_id','film_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_rental');
    }
}
