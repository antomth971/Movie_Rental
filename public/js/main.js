$(document).ready(function() {
    $(".taille").change(function() {
        fileSize = this.files[0].size;
        if (fileSize > MAX_FILE_SIZE) {
            this.setCustomValidity("Le fichier ne doit pas depasser 5MB");
            this.reportValidity();
        } else {
            this.setCustomValidity("");
        }
    });
});

function categoryIndex(value){
    laValeure=value.value
    lesCard=document.querySelectorAll("[dataCateg='"+laValeure+"']")
    cardSup=document.querySelectorAll(".categCard")
    for (let index = 0; index < cardSup.length; index++) {
        const element = cardSup[index];
        element.setAttribute("hidden", true)
    }
    for (let index = 0; index < lesCard.length; index++) {
        const element = lesCard[index];
        element.removeAttribute("hidden")
    }

}
function actorIndex(value){
    laValeure=value.value
    lesCard=document.querySelectorAll("[dataActor='"+laValeure+"']");
    cardSup=document.querySelectorAll(".categCard");
    // for (let index = 0; index < cardSup.length; index++) {
    //     const element = cardSup[index];
    //     element.setAttribute("hidden", true)
    // }
    // for (let index = 0; index < lesCard.length; index++) {
    //     const element = lesCard[index];
    //     element.removeAttribute("hidden")
    // }
}

function filmSearch(film){
    laValeur=film.value;
    lesNoms=document.querySelectorAll(".card-title");
    cardSup=document.querySelectorAll(".categCard");
    for (let index = 0; index < cardSup.length; index++) {
        const element = cardSup[index];
        element.setAttribute("hidden", true)
    }
    var arrayNom=[];
    for (let index = 0; index < lesNoms.length; index++) {
        const element = lesNoms[index].textContent;
        arrayNom.push(element)
    }
    for (let index = 0; index < arrayNom.length; index++) {
        const element2 = arrayNom[index];
        if (element2.match(laValeur) != null) {
            lesCard=document.querySelector("[dataNameSearch='"+element2+"']")
            lesCard.removeAttribute("hidden")
        }
    }
}
function userSearch(user) {
    laValeur=user.value;
    lesNoms=document.querySelectorAll(".lesNoms");
    cardSup=document.querySelectorAll(".lesNoms");
    for (let index = 0; index < cardSup.length; index++) {
        const element = cardSup[index];
        element.setAttribute("hidden", true)
    }
    var arrayNom=[];
    for (let index = 0; index < lesNoms.length; index++) {
        const element = lesNoms[index].id;
        arrayNom.push(element)
    }
    i=1
    for (let index = 0; index < arrayNom.length; index++) {
        const element2 = arrayNom[index];
        if (isNaN(laValeur)) {
        if (element2.toLowerCase().match(laValeur.toLowerCase()) != null) {
            lesCard=document.querySelector("[UserSearch='"+i+"']")
            lesCard.removeAttribute("hidden")
        }
    }
    else{
        if (element2.toLowerCase().match(laValeur.toLowerCase()) != null) {
            lesCard=document.querySelector("[UserSearch='"+i+"']")
            lesCard.removeAttribute("hidden")
        }
    }

        i++
    }
}

function commandSearch(user) {
    laValeur=user.value;
    lesNoms=document.querySelectorAll(".lesNoms");
    cardSup=document.querySelectorAll(".lesNoms");
    for (let index = 0; index < cardSup.length; index++) {
        const element = cardSup[index];
        element.setAttribute("hidden", true)
        var idElement= element.id;
        if (idElement.match(laValeur) !=null) {
            element.removeAttribute("hidden")
        }
    }
}

$('.quantity').on('change', function() {
    $(this).closest('form').submit();
});

$(document).ready(function() {
    $(".selectpicker").selectpicker();
  });