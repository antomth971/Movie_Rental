<?php

use App\Http\Controllers\AdminCategory;
use App\Http\Controllers\AdminCommentary;
use App\Http\Controllers\AdminDirectors;
use App\Http\Controllers\adminProduits;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CartControllerR;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Index;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\supAdmin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->group(function () {

    //Ici les routes ou il est indispensable d'être logged in.
    Route::get('/client',[ClientController::class,'index']);
    Route::get('client/{id}', [ClientController::class,'show'])->name('test');
    Route::get('/client/addComment/{id}',[ClientController::class,'addComment']);
    Route::post('/client/addComment/store',[ClientController::class,'storeComment']);
    Route::get('panier',[CartController::class,'CartList'])->name('cart.list');
    Route::post('panier',[CartController::class,'addToCart'])->name('cart.ajout');
    Route::post('modif-panier',[CartController::class,'updateCart'])->name('cart.modif');
    Route::post('retirer',[CartController::class,'removeCart'])->name('cart.retirer');
    Route::post('vider',[CartController::class,'clearAllCart'])->name('cart.vider');
    Route::get('/valider',[RentalController::class,'finalisationPanier']);
    Route::get('/commands',[RentalController::class,'indexFacture']);
    Route::get('/detail/commande/{id}',[RentalController::class,'detailCommande']);
    Route::get('pdfClient/{id}',[RentalController::class,'CreerPdf']);
});

Route::get('/',[Index::class,'index']);
Route::get('/films/{id}',[Index::class,'show']);



Route::middleware('OnlyAdmin')->group(function () {

    //Ici les routes ou il est indispensable d'être logged in admin.
    Route::resource('/admins', AdminProduits::class);
    Route::resource('/comment',AdminCommentary::class);
    Route::resource('/director',AdminDirectors::class);
    Route::resource('/category',AdminCategory::class);
    Route::get('/commandsAdmin',[RentalController::class,'indexFactureAdmin']);
    Route::get('/detail/commandAdmin/{id}',[RentalController::class,"adminCommandDetail"]);


});

Route::middleware('OnlySupAdmin')->group(function(){
    //Ici les routes ou il est indispensable d'être logged in sup_admin.
    Route::resource('/supAdmin', supAdmin::class);

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

